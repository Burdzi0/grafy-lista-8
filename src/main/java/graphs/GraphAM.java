package graphs;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class GraphAM implements Graph {

    int[][] graph;
    private int SIZE = 10;

    public GraphAM() {
        graph = new int[SIZE][SIZE];
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph[0].length; j++) {
                graph[i][j] = 0;
            }
            graph[0][i] = -1;
            graph[i][0] = -1;
        }
    }

    @Override
    public boolean addVertex(int vertex) {
        if (vertex < 1 || vertex > SIZE)
            return false;
        graph[vertex][0] = 1;
        graph[0][vertex] = 1;
        return true;
    }

    @Override
    public boolean deleteVertex(int vertex) {
        if (vertex < 1 || vertex > SIZE)
            return false;
        if (graph[vertex][0] == -1)
            return false;
        graph[vertex][0] = -1;
        graph[0][vertex] = -1;
        for(int i =1; i<graph[1].length;i++){
            if(graph[vertex][i]!=0){
                graph[i][vertex]=0;
                graph[vertex][i]=0;
            }
        }
        return true;
    }

    @Override
    public boolean addEdge(int vertex0, int vertex1) {
        if (vertex0 < 1 || vertex0 > SIZE || vertex1 < 1 || vertex1 > SIZE)
            return false;
        if (graph[vertex0][0] == -1 || graph[vertex1][0] == -1)
            return false;
        if (graph[vertex0][vertex1] != 0)
            return false;
        graph[vertex0][vertex1] = 1;
        graph[vertex1][vertex0] = 1;
        return true;
    }

    @Override
    public boolean deleteEdge(int vertex0, int vertex1) {
        if (vertex0 < 1 || vertex0 > SIZE || vertex1 < 1 || vertex1 > SIZE)
            return false;
        if (graph[vertex0][0] == -1 || graph[vertex1][0] == -1)
            return false;
        if (graph[vertex0][vertex1] == 0)
            return false;
        graph[vertex0][vertex1] = 0;
        graph[vertex1][vertex0] = 0;
        return true;
    }

    @Override
    public void write() {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileWriter("Graf.txt"));
            for (int i = 0; i < graph.length; i++) {
                for (int j = 0; j < graph[0].length; j++)
                    pw.printf("%3d", graph[i][j]);
                pw.println();
            }

            pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Nic nie drukuje");
        } finally {
            try {
                if (pw != null)
                    pw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void read() {

    }

    @Override
    public void printGraph() {
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph[0].length; j++) {
                System.out.printf("%3d", graph[i][j]);
            }
            System.out.println();
        }
    }

    @Override
    public void printEdges() {

    }

    @Override
    public int changeValueOfEdge(int vertex0, int vertex1, int value) {
        if (vertex0 < 1 || vertex0 > SIZE || vertex1 < 1 || vertex1 > SIZE)
            return -1;
        if (graph[vertex0][0] == -1 || graph[vertex1][0] == -1)
            return -1;
        if (graph[vertex0][vertex1] == 0)
            return -1;
        int last = graph[vertex0][vertex1];
        graph[vertex0][vertex1] = value;
        graph[vertex1][vertex0] = value;
        return last;
    }
}
